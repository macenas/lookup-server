package controller.listeners;

import view.MainWindow;
import lookup.AOR;
import common.interfaces.LookupListener;

public class LookupServerListener implements LookupListener {
	private MainWindow window;
	
	public LookupServerListener(MainWindow window) {
		this.window = window;
	}
	
	@Override
	public void onRegister(String objectName, AOR aor) {
		this.window.addObject(objectName, aor);

	}

	@Override
	public void onUnregister(String objectName) {
		this.window.removeObject(objectName);
	}

}
