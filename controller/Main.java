package controller;

import controller.listeners.LookupServerListener;
import lookup.LookupServer;
import view.MainWindow;

/**
 * 
 * Projeto da disciplina de Tópicos Avançados em Sistemas Distribuídos (if749)
 *
 * SERVIDOR DE LOOKUP
 * 
 * @author Marcelo Nascimento Oliveira
 *
 */
public class Main {
	private static final int PORT = 6000;
	private static MainWindow window;

	public static void main(String[] args) {
		window = new MainWindow();
		LookupServer.getInstance().addListener(
				new LookupServerListener(window));
		LookupServer.getInstance().start(Main.PORT);
		window.alignAndShow();
	}
	
}