package view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Utils {
	
	public static void alignAndShow(JFrame frame) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = frame.getSize();
		int x = screenSize.width/2-frameSize.width/2;
		int y = screenSize.height/2-frameSize.height/2;
		frame.setLocation(x, y);
		frame.setVisible(true);
	}
	
}
