package view;
	
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import lookup.AOR;
	
public class MainWindow extends JFrame {
	private static final long serialVersionUID = 5082910056381791106L;
	private JPanel titlePanel;
	private JLabel titleLabel;
	private DefaultTableModel objectsTableModel;
	private JTable objectsTable;
	private JScrollPane objectsScrollPane;
	
	public MainWindow() {
		super("Projeto de TASD - Servidor de lookup");
		
		// Sets "look and fell" to system default.
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		// Creates UI elements.
		String[] columnNames = {"Nome", "Endereço IP", "Porta", "ID do objeto"};
		this.titlePanel = new JPanel();
		this.titleLabel = new JLabel("Objetos registrados");
		this.objectsTableModel = new DefaultTableModel(columnNames, 0);
		this.objectsTable = new JTable(this.objectsTableModel);
		this.objectsScrollPane = new JScrollPane(this.objectsTable);
		
		// Adjusts UI elements.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.setLayout(new BorderLayout());
		this.objectsTable.setEnabled(false);
		this.titleLabel.setFont(this.titleLabel.getFont().deriveFont(25.0f));
		
		// Adds UI elements.
		this.add(this.titlePanel, BorderLayout.NORTH);
		this.add(this.objectsScrollPane, BorderLayout.CENTER);
		this.titlePanel.add(this.titleLabel);
	}
	
	public void alignAndShow() {
		Utils.alignAndShow(this);
	}

	public void addObject(String objectName, AOR aor) {
		String port = Integer.toString(aor.getPort());
		String id = Integer.toString(aor.getId());
		String[] line = {objectName, aor.getAddress(), port, id};
		((DefaultTableModel) this.objectsTable.getModel()).addRow(line);
	}
	
	public void removeObject(String objectName) {
		
		for (int i = 0; i < this.objectsTable.getModel().getRowCount(); i++) {
			
			if (((DefaultTableModel) this.objectsTable.getModel()).
					getValueAt(i, 0).equals(objectName)) {
				((DefaultTableModel) this.objectsTable.getModel()).removeRow(i);
				break;
			}
			
		}
		
	}
	
}
